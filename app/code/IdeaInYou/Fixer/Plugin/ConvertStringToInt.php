<?php


namespace IdeaInYou\Fixer\Plugin;

use Magento\Catalog\Model\Product\Image\ParamsBuilder;

class ConvertStringToInt
{
    /**
     * @param ParamsBuilder $subject
     * @param array $imageArguments
     * @param int|null $scopeId
     * @return array
     */
    public function beforeBuild(ParamsBuilder $subject, array $imageArguments, int $scopeId = null)
    {
        if (isset($imageArguments["width"]))
            $imageArguments["width"] = intval($imageArguments["width"]);

        if (isset($imageArguments["height"]))
            $imageArguments["height"] = intval($imageArguments["height"]);

        return [$imageArguments, $scopeId];
    }
}
