<?php
namespace IdeaInYou\Doctor\Model;

use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;
use IdeaInYou\Doctor\Api\Data;
use IdeaInYou\Doctor\Model\ResourceModel\Doctor as ResourceModel;
use IdeaInYou\Doctor\Model\ResourceModel\Doctor\CollectionFactory as DoctorCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Class DoctorRepository
 * @package IdeaInYou\Doctor
 */
class DoctorRepository implements DoctorRepositoryInterface
{
    /**
     * @var ResourceModel
     */
    protected $resource;

    /**
     * @var DoctorFactory
     */
    protected $doctorFactory;

    /**
     * @var DoctorCollectionFactory
     */
    protected $doctorCollectionFactory;

    /**
     * @var Data\DoctorSearchResultsInterface
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magento\Cms\Api\Data\DoctorInterfaceFactory
     */
    protected $dataDoctorFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param ResourceModel $resource
     * @param DoctorFactory $doctorFactory
     * @param DoctorCollectionFactory $doctorCollectionFactory
     * @param Data\DoctorSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceModel $resource,
        DoctorFactory $doctorFactory,
        DoctorCollectionFactory $doctorCollectionFactory,
        Data\DoctorSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->doctorFactory = $doctorFactory;
        $this->doctorCollectionFactory = $doctorCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * {@inheritDoc}
     */
    public function save(Data\DoctorInterface $doctor)
    {
        try {
            $this->resource->save($doctor);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $doctor;
    }

    /**
     * {@inheritDoc}
     */
    public function getById($doctorId)
    {
        $doctor = $this->doctorFactory->create();
        $this->resource->load($doctor, $doctorId);
        $doctor->setPhoto(json_decode($doctor->getPhoto()));
        if (!$doctor->getId()) {
            throw new NoSuchEntityException(__('Doctor with id "%1" does not exist.', $doctorId));
        }
        return $doctor;
    }

    /**
     * {@inheritDoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \IdeaInYou\doctor\Model\ResourceModel\doctor\Collection $collection */
        $collection = $this->doctorCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\DoctorSearchResultsInterfaceFactory $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(Data\DoctorInterface $doctor)
    {
        try {
            $this->resource->delete($doctor);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function deleteById($doctorId)
    {
        return $this->delete($this->getById($doctorId));
    }
}
