<?php
namespace IdeaInYou\Doctor\Model\ResourceModel\Doctor;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package IdeaInYou\Doctor
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'doctor_id';

    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'IdeaInYou\Doctor\Model\Doctor',
            'IdeaInYou\Doctor\Model\ResourceModel\Doctor'
        );
    }
}
