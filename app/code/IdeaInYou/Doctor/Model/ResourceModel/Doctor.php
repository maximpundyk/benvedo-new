<?php
namespace IdeaInYou\Doctor\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Doctor
 * Resource Model
 * @package IdeaInYou\Doctor
 */
class Doctor extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('ideaInYou_doctor', 'doctor_id');
    }
}
