<?php
namespace IdeaInYou\Doctor\Model;

use Magento\Framework\Model\AbstractModel;
use IdeaInYou\Doctor\Api\Data\DoctorInterface;

/**
 * Class Doctor
 * @package IdeaInYou\Doctor
 */
class Doctor extends AbstractModel implements DoctorInterface
{
    /**
     * Set resource model
     */
    protected function _construct()
    {
        $this->_init('IdeaInYou\Doctor\Model\ResourceModel\Doctor');
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->getData(self::DOCTOR_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return $this->getData(self::FULL_NAME);
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

//    /**
//     * {@inheritDoc}
//     */
//    public function getFoundationYear()
//    {
//        return $this->getData(self::FOUNDATION_YEAR);
//    }

    /**
     * {@inheritDoc}
     */
    public function getImage()
    {
        return $this->getData(self::PHOTO);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setId($doctor_id)
    {
        return $this->setData(self::DOCTOR_ID, $doctor_id);
    }

    /**
     * {@inheritDoc}
     */
    public function setName($full_name)
    {
        return $this->setData(self::FULL_NAME, $full_name);
    }

    /**
     * {@inheritDoc}
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

//    /**
//     * {@inheritDoc}
//     */
//    public function setFoundationYear($foundation_year)
//    {
//        return $this->setData(self::FOUNDATION_YEAR, $foundation_year);
//    }

    /**
     * {@inheritDoc}
     */
    public function setImage($photo)
    {
        return $this->setData(self::PHOTO, $photo);
    }
}
