<?php
namespace IdeaInYou\Doctor\Model\Product\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use IdeaInYou\Doctor\Model\ResourceModel\Doctor\CollectionFactory;
use IdeaInYou\Doctor\Api\Data\DoctorInterface;

/**
 * Class Doctor
 * @package IdeaInYou\Doctor
 */
class Doctor extends AbstractSource
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [];

            $collection = $this->collectionFactory->create();

            /** @var DoctorInterface $doctor */
            foreach ($collection as $doctor) {
                $this->_options[] = ['label' => __($doctor->getName()), 'value' => $doctor->getId()];
            }
        }
        return $this->_options;
    }
}
