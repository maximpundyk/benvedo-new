<?php
namespace IdeaInYou\Doctor\Model\Doctor;

use IdeaInYou\Doctor\Api\Data\DoctorInterface;
use IdeaInYou\Doctor\Model\ResourceModel\Doctor\CollectionFactory;
use IdeaInYou\Doctor\Model\ResourceModel\Doctor\Collection as DoctorCollection;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\UrlInterface;

/**
 * Class DataProvider
 * @package IdeaInYou\Doctor
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var DoctorCollection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Url Builder
     *
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $doctorCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param UrlInterface $urlBuilder
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $doctorCollectionFactory,
        DataPersistorInterface $dataPersistor,
        UrlInterface $urlBuilder,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $doctorCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var DoctorInterface $doctor */
        foreach ($items as $doctor) {
            $this->loadedData[$doctor->getId()] = $this->prepareData($doctor);
        }

        $data = $this->dataPersistor->get('doctor');
        if (!empty($data)) {
            $doctor = $this->collection->getNewEmptyItem();
            $doctor->setData($data);
            $this->loadedData[$doctor->getId()] = $this->prepareData($doctor);
            $this->dataPersistor->clear('doctor');
        }

        return $this->loadedData;
    }

    /**
     * @param DoctorInterface $doctor
     */
    private function prepareData($doctor)
    {
        $data = $doctor->getData();

        if (isset($data['logo'])) {
            unset($data['logo']);
            $data['logo'][0]['name'] = $doctor->getData('logo');
            $data['logo'][0]['url'] = $this->getFileUrl($doctor->getLogo());
        }

        return $data;
    }

    /**
     * @param string $fileName
     * @return string
     */
    private function getFileUrl($fileName)
    {
        return $this->urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . 'doctor/' . $fileName;
    }
}
