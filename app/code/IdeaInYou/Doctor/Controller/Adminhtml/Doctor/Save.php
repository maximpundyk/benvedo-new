<?php
namespace IdeaInYou\Doctor\Controller\Adminhtml\Doctor;

use Magento\Backend\App\Action\Context;
use IdeaInYou\Doctor\Api\Data\DoctorInterface;
use IdeaInYou\Doctor\Model\DoctorFactory;
use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class Save
 * @package IdeaInYou\Doctor
 */
class Save extends AbstractAction
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var DoctorRepositoryInterface
     */
    private $doctorRepository;

    /**
     * @var DoctorFactory
     */
    private $doctorFactory;

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * @param Context $context
     * @param DoctorRepositoryInterface $doctorRepository
     * @param DoctorFactory $doctorFactory
     * @param DataPersistorInterface $dataPersistor
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        DoctorRepositoryInterface $doctorRepository,
        DoctorFactory $doctorFactory,
        DataPersistorInterface $dataPersistor,
        ImageUploader $imageUploader
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->doctorRepository = $doctorRepository;
        $this->doctorFactory = $doctorFactory;
        $this->imageUploader = $imageUploader;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('doctor_id');

            if (empty($data['doctor_id'])) {
                $data['doctor_id'] = null;
            }

            if ($id) {
                try {
                    /** @var DoctorInterface $model */
                    $model = $this->doctorRepository->getById($id);
                } catch (NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                    /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                    $resultRedirect = $this->resultRedirectFactory->create();
                    return $resultRedirect->setPath('*/*/');
                }
            } else {
                /** @var DoctorInterface $model */
                $model = $this->doctorFactory->create();
            }
            //$this->processImage($data);
            $model->setData($data);

            try {
                $model->setPhoto(json_encode($model->getPhoto()));
                $this->doctorRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the doctor.'));
                $this->dataPersistor->clear('doctor');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['doctor_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the doctor.'));
            }

            $this->dataPersistor->set('doctor', $data);
            return $resultRedirect->setPath('*/*/edit', ['doctor_id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return array
     */
    private function processImage(&$data)
    {
        $this->filePreprocessing($data, 'photo');
        $this->filterFileData($data, 'photo');
        $this->moveImage($data, 'photo');
    }

    /**
     * @param array $data
     * @param string $fieldName
     */
    private function filterFileData(&$data, $fieldName)
    {
        if (isset($data[$fieldName]) && is_array($data[$fieldName])) {
            if (!empty($data[$fieldName]['delete'])) {
                $data[$fieldName] = null;
            } else {
                if (isset($data[$fieldName][0]['full_name']) && isset($data[$fieldName][0]['tmp_name'])) {
                    $data[$fieldName] = $data[$fieldName][0]['full_name'];
                } else {
                    unset($data[$fieldName]);
                }
            }
        }
    }

    /**
     * @param array $data
     * @param string $fieldName
     */
    private function filePreprocessing(&$data, $fieldName)
    {
        if (empty($data[$fieldName])) {
            unset($data[$fieldName]);
            $data[$fieldName]['delete'] = true;
        }
    }

    /**
     * @param array $data
     * @param string $fieldName
     */
    private function moveImage(&$data, $fieldName)
    {
        if ($data[$fieldName]) {
            $this->imageUploader->moveFileFromTmp($data[$fieldName]);
        }
    }
}
