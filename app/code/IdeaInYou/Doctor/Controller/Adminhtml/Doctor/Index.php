<?php
namespace IdeaInYou\Doctor\Controller\Adminhtml\Doctor;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Index
 * @package IdeaInYou\Doctor
 */
class Index extends AbstractAction
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $resultPage->setActiveMenu('IdeaInYou_Doctor::doctors');
        $resultPage->getConfig()->getTitle()->set(__("Doctors")); // set title in head
        $resultPage->getConfig()->getTitle()->prepend(__("Doctors")); // set title on page

        return $resultPage;
    }
}
