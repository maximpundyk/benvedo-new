<?php
namespace IdeaInYou\Doctor\Controller\Adminhtml\Doctor;

use Magento\Backend\App\Action\Context;
use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Delete
 */
class Delete extends AbstractAction
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var DoctorRepositoryInterface
     */
    private $doctorRepository;

    /**
     * @param Context $context
     * @param DoctorRepositoryInterface $doctorRepository
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DoctorRepositoryInterface $doctorRepository,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->DoctorRepository = $doctorRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('doctor_id');

        try {
            $this->doctorRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('You deleted the doctor'));
            $this->dataPersistor->clear('doctor');
            return $resultRedirect->setPath('*/*/');
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting the doctor.'));
        }

        return $resultRedirect->setPath('*/*/');
    }
}
