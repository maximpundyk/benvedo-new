<?php
namespace IdeaInYou\Doctor\Controller\Adminhtml\Doctor;

use Magento\Backend\App\Action\Context;
use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;
use IdeaInYou\Doctor\Model\ResourceModel\Doctor\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class MassDelete
 * @package IdeaInYou\Doctor
 */
class MassDelete extends AbstractAction
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var DoctorRepositoryInterface
     */
    private $doctorRepository;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param Context $context
     * @param DoctorRepositoryInterface $doctorRepository
     * @param CollectionFactory $collectionFactory
     * @param Filter $filter
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DoctorRepositoryInterface $doctorRepository,
        CollectionFactory $collectionFactory,
        Filter $filter,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->filter = $filter;
        $this->DoctorRepository = $doctorRepository;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        try {
            foreach ($collection as $doctor) {
                $this->doctorRepository->delete($doctor);
            }
            $message = __('A total of %1 record(s) have been deleted.', $collection->getSize());
            $this->messageManager->addSuccessMessage($message);
            $this->dataPersistor->clear('doctor');
            return $resultRedirect->setPath('*/*/');
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting doctors.'));
        }

        return $resultRedirect->setPath('*/*/');
    }
}
