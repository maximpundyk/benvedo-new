<?php
namespace IdeaInYou\Doctor\Controller\Adminhtml\Doctor;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultInterface;
use IdeaInYou\Doctor\Model\DoctorFactory;
use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Edit
 * @package IdeaInYou\Doctor
 */
class Edit extends AbstractAction
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var DoctorRepositoryInterface
     */
    private $doctorRepository;

    /**
     * @var DoctorFactory
     */
    private $doctorFactory;

    /**
     * @param Context $context
     * @param DoctorRepositoryInterface $doctorRepository
     * @param DoctorFactory $doctorFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        DoctorRepositoryInterface $doctorRepository,
        DoctorFactory $doctorFactory,
        PageFactory $resultPageFactory
    ) {
        $this->doctorRepository = $doctorRepository;
        $this->doctorFactory = $doctorFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('doctor_id');

        if ($id) {
            try {
                $model = $this->doctorRepository->getById($id);
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $model = $this->doctorFactory->create();
        }

        /** @var ResultInterface $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('IdeaInYou_Doctor::doctors');

        $resultPage->getConfig()->getTitle()->prepend(__('Doctor'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getName() : __('New Doctor'));
        return $resultPage;
    }
}
