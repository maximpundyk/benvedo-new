<?php
namespace IdeaInYou\Doctor\Controller\Adminhtml\Doctor;

use Magento\Backend\App\Action\Context;
use IdeaInYou\Doctor\Api\Data\DoctorInterface;
use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class InlineEdit
 * @package IdeaInYou\Doctor
 */
class InlineEdit extends AbstractAction
{

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonFactory;

    /**
     * @var DoctorRepositoryInterface
     */
    private $doctorRepository;

    /**
     * @param Context $context
     * @param DoctorRepositoryInterface $doctorRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        DoctorRepositoryInterface $doctorRepository,
        JsonFactory $jsonFactory
    ) {
        $this->doctorRepository = $doctorRepository;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (empty($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $id) {
                    try {
                        /** @var DoctorInterface $model */
                        $model = $this->doctorRepository->getById($id);
                        $model->setData(array_merge($model->getData(), $postItems[$id]));
                        $this->doctorRepository->save($model);
                    } catch (NoSuchEntityException $e) {
                        $messages[] = $e->getMessage();
                        $error = true;
                    } catch (CouldNotSaveException $e) {
                        $messages[] = $e->getMessage();
                        $error = true;
                    } catch (\Exception $e) {
                        $messages[] = $e->getMessage();
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
