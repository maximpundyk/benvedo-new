<?php
namespace IdeaInYou\Doctor\Controller\Adminhtml\Doctor;

use Magento\Backend\App\Action;

/**
 * Class AbstractAction
 * @package IdeaInYou\Doctor
 */
abstract class AbstractAction extends Action
{
    const ADMIN_RESOURCE = 'IdeaInYou_Doctor::doctor';
}
