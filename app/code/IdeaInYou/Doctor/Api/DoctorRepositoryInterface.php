<?php
namespace IdeaInYou\Doctor\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * IdeaInYou DoctorRepository interface.
 * @package IdeaInYou\Doctor
 * @api
 */
interface DoctorRepositoryInterface
{
    /**
     * Save doctor.
     *
     * @param \IdeaInYou\Doctor\Api\Data\DoctorInterface $doctor
     * @return \IdeaInYou\Doctor\Api\Data\DoctorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\DoctorInterface $doctor);

    /**
     * Retrieve Doctor.
     *
     * @param int $doctorId
     * @return \IdeaInYou\Doctor\Api\Data\DoctorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($doctorId);

    /**
     * Get Doctors list.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResults
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete doctor.
     *
     * @param \IdeaInYou\Doctor\Api\Data\DoctorInterface $doctor
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\DoctorInterface $doctor);

    /**
     * Delete doctor by ID.
     *
     * @param int $doctorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($doctorId);
}
