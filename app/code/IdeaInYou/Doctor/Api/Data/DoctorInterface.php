<?php
namespace IdeaInYou\Doctor\Api\Data;

/**
 * IdeaInYou Doctor interface
 * @package IdeaInYou\Doctor
 * @api
 */
interface DoctorInterface
{
    const DOCTOR_ID = 'doctor_id';
    const FULL_NAME = 'full_name';
    const ADDRESS = 'address';
//    const FOUNDATION_YEAR = 'foundation_year';
    const PHOTO = 'photo';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Name
     *
     * @return string
     */
    public function getName();

    /**
     * Get Country
     *
     * @return string|null
     */
    public function getAddress();

//    /**
//     * Get Foundation Year
//     *
//     * @return int|null
//     */
//    public function getFoundationYear();

    /**
     * Get Photo
     *
     * @return string|null
     */
    public function getImage();

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get Updated At
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set ID
     *
     * @param int $id
     * @return DoctorInterface
     */
    public function setId($id);

    /**
     * Set Name
     *
     * @param string $full_name
     * @return DoctorInterface
     */
    public function setName($full_name);

    /**
     * Set Country
     *
     * @param string $address
     * @return DoctorInterface
     */
    public function setAddress(string $address);

//    /**
//     * Set Foundation Year
//     *
//     * @param string $foundation_year
//     * @return DoctorInterface
//     */
//    public function setFoundationYear($foundation_year);

    /**
     * Set Photo
     *
     * @param string $photo
     * @return DoctorInterface
     */
    public function setImage($photo);
}
