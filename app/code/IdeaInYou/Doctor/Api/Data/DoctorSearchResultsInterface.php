<?php
namespace IdeaInYou\Doctor\Api\Data;

/**
 * IdeaInYou Doctor search results interface.
 * @package IdeaInYou\Doctor
 * @api
 */
interface DoctorSearchResultsInterface
{
    /**
     * Get doctor list.
     *
     * @return \IdeaInYou\Doctor\Api\Data\DoctorInterfaceInterface[]
     */
    public function getItems();

    /**
     * Set brands list.
     *
     * @param \IdeaInYou\Doctor\Api\Data\DoctorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
