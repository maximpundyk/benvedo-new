<?php
namespace IdeaInYou\Doctor\Block\Adminhtml\Buttons;

use Magento\Backend\Block\Widget\Context;
use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 * @package IdeaInYou\Doctor
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var DoctorRepositoryInterface
     */
    private $doctorRepository;

    /**
     * @param Context $context
     * @param DoctorRepositoryInterface $doctorRepository
     */
    public function __construct(
        Context $context,
        DoctorRepositoryInterface $doctorRepository
    ) {
        $this->context = $context;
        $this->doctorRepository = $doctorRepository;
    }

    /**
     * @return int|null
     */
    public function getDoctorId()
    {
        try {
            return $this->doctorRepository->getById(
                $this->context->getRequest()->getParam('doctor_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
