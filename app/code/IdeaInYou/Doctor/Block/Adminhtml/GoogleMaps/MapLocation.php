<?php


namespace IdeaInYou\Doctor\Block\Adminhtml\GoogleMaps;

use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\View\Element\Template;


class MapLocation extends Template
{
    /**
     * @var EavConfig
     */
    protected $eavConfig;

    const CUSTOM_COLOR_ATTR = "custom_color";

    public function __construct(
        Template\Context $context,
        EavConfig $eavConfig,
        array $data = []
    ) {
        $this->eavConfig = $eavConfig;
        parent::__construct($context, $data);
    }

    /**
     * Return ajax url for button.
     *
     * @return string
     */
    public function getGoogleApiKey()
    {
        return 'AIzaSyBFnaVHOjblHLv0y_V_mQdVEIlUWCKZQUI';
    }
}
