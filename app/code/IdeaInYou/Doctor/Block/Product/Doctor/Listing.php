<?php
namespace IdeaInYou\Doctor\Block\Product\Doctor;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use IdeaInYou\Doctor\Api\DoctorRepositoryInterface;

/**
 * Class Listing
 * @package IdeaInYou\Doctor
 */
class Listing extends Template
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var DoctorRepositoryInterface
     */
    private $doctorRepository;

    /**
     * @var Product
     */
    protected $product = null;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @param Template\Context $context
     * @param Registry $registry
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param DoctorRepositoryInterface $doctorRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        DoctorRepositoryInterface $doctorRepository,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->doctorRepository = $doctorRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\Api\SearchResults
     */
    public function getList()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('doctor_id', $this->getProduct()->getDoctors(), 'in')
            ->create();
        $list = $this->doctorRepository->getList($searchCriteria);
        return $list;
    }

    /**
     * @param string $logoFileName
     * @return string
     */
    public function getLogoUrl($logoFileName)
    {
        return $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . 'doctor/' . $logoFileName;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    protected function getProduct()
    {
        if (!$this->product) {
            $this->product = $this->coreRegistry->registry('product');
        }
        return $this->product;
    }
}
