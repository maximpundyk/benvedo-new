define([
    'Magento_Ui/js/form/element/abstract',
    'ko',
    "jquery",
    "googleMapPlaceLibrary"
], function (Abstract, ko, $) {

    return Abstract.extend({
        hiddenAddressInputId: "address_data",

        initialize: function () {
            this._super();
        },

        initAutocomplete: function () {
            let self = this;
            self.autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById(self.uid)),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            self.autocomplete.addListener('place_changed', $.proxy(self.fillInAddress, self));
        },

        fillInAddress: function () {
            let self = this;
            // Get the place details from the autocomplete object.
            let place = self.autocomplete.getPlace();

            let placeToParse = {
                address_components: place.address_components,
                place_id: place.place_id,
                id: place.id,
                coordinates: {
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng()
                },
                formatted_address: place.formatted_address,
                url: place.url,
                website: place.website,
                vicinity: place.vicinity
            }

            let elem = $("input[name='"+self.hiddenAddressInputId+"']");

            elem.attr('value', JSON.stringify(placeToParse));
            elem.trigger('change');
            $("#" + self.uid).trigger("change");

        },

        geolocate: function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    let geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    let circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    self.autocomplete.setBounds(circle.getBounds());
                });
            }
        },

        lol: function () {
            alert("LOL!!!")
        }
    });
});
