<?php
namespace IdeaInYou\Doctor\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package IdeaInYou\Doctor
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = 'ideaInYou_doctor';
        $installer->getConnection()->dropTable($installer->getTable($tableName));

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'doctor_id',
            Table::TYPE_INTEGER,
            10,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Entity Id'
        )->addColumn(
            'full_name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Full Name'
        )->addColumn(
            'address',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Address'
        )->addColumn(
            'address_data',
            Table::TYPE_TEXT,
            2048,
            ['nullable' => false],
            'Address Data'
        )->addColumn(
            'locality_political',
            Table::TYPE_TEXT,
            126,
            ['nullable' => false],
            'Address Data'
        )->addColumn(
            'country_political',
            Table::TYPE_TEXT,
            2048,
            ['nullable' => false],
            'Address Data'
        )
//            ->addColumn(
//            'foundation_year',
//            Table::TYPE_INTEGER,
//            null,
//            ['nullable' => true],
//            'Foundation_Year'
//        )
            ->addColumn(
            'photo',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Photo'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Create date'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Update date'
        )->addIndex(
            $installer->getIdxName(
                $tableName,
                ['full_name', 'address'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['full_name', 'address'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'Doctors'
        );

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
