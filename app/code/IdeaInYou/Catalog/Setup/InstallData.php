<?php

namespace IdeaInYou\Catalog\Setup;

use \Magento\Eav\Setup\EavSetup;
use \Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Setup\InstallDataInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;

use Magento\Eav\Model\Entity\TypeFactory as EavTypeFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\AttributeSetManagement;
use Magento\Eav\Model\AttributeManagement;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Catalog\Api\AttributeSetRepositoryInterface;
use Magento\Eav\Api\AttributeGroupRepositoryInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;

class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * @var AttributeManagement
     */
    private $attributeManagement;
    /**
     * @var AttributeSetManagement
     */
    private $attributeSetManagement;
    /**
     * @var AttributeSetFactoryCollectionFactory
     */
    private $attributeSetFactory;
    /**
     * @var EavTypeFactory
     */
    private $eavTypeFactory;

    const CONTACT_LENSES_SET = "Contact Lenses";

    const SUNGLASSES_SET = "Sunglasses";

    const EYEGLASSES_SET = "Eyeglasses";

    const CARE_PRODUCT_SET = "Care Products";

    const BASE_ATTRIBUTE_SET = "Configurable";
    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param EavTypeFactory $eavTypeFactory
     * @param AttributeSetFactory $attributeSetFactory
     * @param AttributeSetManagement $attributeSetManagement
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param AttributeGroupRepositoryInterface $attributeGroupRepository
     * @param AttributeManagement $attributeManagement
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        EavTypeFactory $eavTypeFactory,
        AttributeSetFactory $attributeSetFactory,
        AttributeSetManagement $attributeSetManagement,
        AttributeSetRepositoryInterface $attributeSetRepository,
        AttributeGroupRepositoryInterface $attributeGroupRepository,
        AttributeManagement $attributeManagement,
        CollectionFactory $collectionFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavTypeFactory = $eavTypeFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->attributeManagement = $attributeManagement;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->createAttributeSets();

        $setup->endSetup();
    }

    public function createAttributeSets()
    {
        $entityTypeCode = 'catalog_product';
        $entityType = $this->eavTypeFactory->create()->loadByCode($entityTypeCode);

        $attributeSetCollection = $this->collectionFactory->create()
            ->addFieldToFilter("attribute_set_name", self::BASE_ATTRIBUTE_SET)
            ->addFieldToFilter("entity_type_id", $entityType->getId());

        if ($attributeSetCollection->count() > 0) {
            $baseAttributeSetId = $attributeSetCollection->getFirstItem()->getAttributeSetId();
        } else {
            $baseAttributeSetId = $entityType->getDefaultAttributeSetId();
        }

        $dataArray = [
            [
                'attribute_set_name' => self::CONTACT_LENSES_SET,
                'entity_type_id' => $entityType->getId(),
                'sort_order' => 100,
            ],
            [
                'attribute_set_name' => self::SUNGLASSES_SET,
                'entity_type_id' => $entityType->getId(),
                'sort_order' => 110,
            ],
            [
                'attribute_set_name' => self::EYEGLASSES_SET,
                'entity_type_id' => $entityType->getId(),
                'sort_order' => 120,
            ],
            [
                'attribute_set_name' => self::CARE_PRODUCT_SET,
                'entity_type_id' => $entityType->getId(),
                'sort_order' => 130,
            ]
        ];

        foreach ($dataArray as $data) {
            /** @var AttributeSetInterface $attributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeSet->setData($data);

            try {
                $attributeSet->validate();
                $this->attributeSetRepository->save($attributeSet);
                $attributeSet->initFromSkeleton($baseAttributeSetId);
                $this->attributeSetRepository->save($attributeSet);

            } catch (InputException $e) {
            } catch (NoSuchEntityException $e) {
            } catch (LocalizedException $e) {
            }
        }
    }
}