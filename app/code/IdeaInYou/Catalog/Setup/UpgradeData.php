<?php

namespace IdeaInYou\Catalog\Setup;

use IdeaInYou\Catalog\Logger\Logger;
use IdeaInYou\Catalog\Model\Config\Brands;
use Magento\Eav\Model\AttributeManagement;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection as AttributeSetCollection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory as AttributeSetCollectionFactory;
use Magento\Catalog\Model\Product;

use IdeaInYou\Catalog\Model\Config\AttributeCodes;
use TemplateMonster\ShopByBrand\Model\BrandFactory;
use TemplateMonster\ShopByBrand\Api\BrandRepositoryInterface;
use Magento\Eav\Model\Entity\TypeFactory as EavTypeFactory;

class UpgradeData implements UpgradeDataInterface
{


    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var AttributeSetCollectionFactory
     */
    private $attributeSetCollectionFactory;

    /**
     * @var AttributeManagement
     */
    private $attributeManagement;


    /**
     * @var ThemeCollection
     */
    private $eavTypeFactory;

    /**
     * @var EavTypeFactory
     */
    private $brandFactory;

    /**
     * @var BrandRepositoryInterface
     */
    private $brandRepository;

    /**
     * @var Logger
     */
    private $logger;


    public function __construct(
        EavSetupFactory $eavSetupFactory,
        EavTypeFactory $eavTypeFactory,
        AttributeSetCollectionFactory $attributeSetCollectionFactory,
        BrandFactory $brandFactory,
        AttributeManagement $attributeManagement,
        BrandRepositoryInterface $brandRepository,
        Logger $logger
    )
    {
        $this->attributeSetCollectionFactory = $attributeSetCollectionFactory;
        $this->attributeManagement = $attributeManagement;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavTypeFactory = $eavTypeFactory;
        $this->brandFactory = $brandFactory;
        $this->brandRepository = $brandRepository;
        $this->logger = $logger;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addOpticsAttributes($setup);
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addBrands($setup);
            $this->populateAllAttributeSet($setup);
        }

        $setup->endSetup();
    }

    public function populateAllAttributeSet(ModuleDataSetupInterface $setup) {
        $attributeSetNames = [
            InstallData::SUNGLASSES_SET,
            InstallData::EYEGLASSES_SET,
            InstallData::CONTACT_LENSES_SET,
            InstallData::CARE_PRODUCT_SET
        ];

        foreach ($attributeSetNames as $attrSetName) {
            $this->populateAttributeSet($setup, $attrSetName);
        }
    }


    public function populateAttributeSet(ModuleDataSetupInterface $setup, $setName) {
        $attributeSetCodes = [];
        switch ($setName){
            case InstallData::SUNGLASSES_SET: {
                $attributeSetCodes = AttributeCodes::getGlassesAttributeSetCodes();
                break;
            }
            case InstallData::EYEGLASSES_SET: {
                $attributeSetCodes = AttributeCodes::getGlassesAttributeSetCodes();
                unset($attributeSetCodes[AttributeCodes::LENS_COLOR_ATTRIBUTE_CODE]);
                break;
            }
            case InstallData::CONTACT_LENSES_SET: {
                $attributeSetCodes = AttributeCodes::getContactLensesAttributeSetCodes();
                break;
            }
            case InstallData::CARE_PRODUCT_SET: {
                $attributeSetCodes = AttributeCodes::getCareProductAttributeSetCodes();
                break;
            }
        }

        /** @var AttributeSetCollection $attributeSetCollection */
        $attributeSetCollection = $this->attributeSetCollectionFactory->create()
            ->addFieldToFilter("attribute_set_name", $setName)
            ->addFieldToSelect("attribute_set_id");

        if ($attributeSetCollection->count()) {
            $attributeSet = $attributeSetCollection->getFirstItem();

            $entityTypeCode = 'catalog_product';
            $entityType = $this->eavTypeFactory->create()->loadByCode($entityTypeCode);


            $attributeGroupName = 'General';
            $entityTypeId = $entityType->getId();
            $attributeSetId = $attributeSet->getAttributeSetId();


            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttributeGroup(
                $entityTypeId,
                $attributeSetId,
                $attributeGroupName,
                1 // sort order
            );

            $attributeGroupId = $eavSetup->getAttributeGroupId($entityTypeId, $attributeSetId, $attributeGroupName);

            foreach ($attributeSetCodes as $attributeCode) {
                $eavSetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $attributeGroupId,
                    $attributeCode
                );
            }
        }
    }

    public function addOpticsAttributes(ModuleDataSetupInterface $setup) {
        $attributes = AttributeCodes::getAttributes();

        foreach ($attributes as $code => $options) {
            $generalOptions = AttributeCodes::getAttributesGeneralOptions();

            $options = array_merge($options, $generalOptions);
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                Product::ENTITY,
                $code,
                $options
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    public function addBrands(ModuleDataSetupInterface $setup)
    {
        foreach (Brands::getBrands() as $brand) {
            $newBrand = $this->brandFactory->create()->setData($brand);

            try {
                $this->brandRepository->save($newBrand);
            } catch (LocalizedException $exception) {
                $this->logger->addCritical("Brand could not been saved! ".$exception->getMessage(), ["barand_name" => $brand["name"]]);
            }
        }
    }
}

