<?php

namespace IdeaInYou\Catalog\Block;

use Magento\Framework\View\Element\Template;


class Index extends Template
{
    const SCHEDULE_VISIT_ADDRESS_ROUTE = "bpp/index/schedulevisitaddress";
    const SEND_EMAIL_ROUTE = "bpp/index/sendemail";

    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getScheduleVisitAddressRoute() {
        return $this->_urlBuilder->getUrl(
            self::SCHEDULE_VISIT_ADDRESS_ROUTE
        );
    }

    public function getSendEmailRoute() {
        return $this->_urlBuilder->getUrl(
            self::SEND_EMAIL_ROUTE
        );
    }
}