<?php

namespace IdeaInYou\Catalog\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Eav\Model\Config as EavConfig;

class Index54 extends Template
{
    /**
     * @var EavConfig
     */
    protected $eavConfig;

    const CUSTOM_COLOR_ATTR = "custom_color";

    public function __construct(
        Template\Context $context,
        EavConfig $eavConfig,
        array $data = []
    ) {
        $this->eavConfig = $eavConfig;
        parent::__construct($context, $data);
    }

    /**
     * @param $code
     * @return \Magento\Eav\Model\Entity\Attribute\AbstractAttribute|\Magento\Eav\Model\Entity\Attribute\AttributeInterface
     */
    public function getProductAttributeByCode($code) {
        try {
            return $this->eavConfig->getAttribute('catalog_product', $code);
        } catch (LocalizedException $exception) {
            return null;
        }
    }

    /**
     * Return ajax url for button.
     *
     * @return string
     */
    public function getGoogleApiKey()
    {
        return 'AIzaSyBFnaVHOjblHLv0y_V_mQdVEIlUWCKZQUI';
    }
}