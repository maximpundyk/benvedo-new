<?php

namespace IdeaInYou\Catalog\Logger;

class Logger extends \Monolog\Logger
{
    public function __construct($name, array $handlers = array(), array $processors = array())
    {
        parent::__construct($name, $handlers, $processors);
    }
}
