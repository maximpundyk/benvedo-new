<?php

namespace IdeaInYou\Catalog\Controller\Index;

use IdeaInYou\Catalog\Model\Session;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\DataObject;

class ScheduleVisitAddress extends Action implements ActionInterface
{
    const PARAM_MISSED_MESSAGE = "Error: %1 parameter is missed!";
    const POSTAL_CODE_1_PARAM_NAME = "postal_code_1";
    const POSTAL_CODE_2_PARAM_NAME = "postal_code_2";
    const ADDRESS_PARAM_NAME = "address";
    protected $session;

    public function __construct(
        Session $session,
        Context $context
    )
    {
        $this->session = $session;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $address = $this->validatedRequestedAddress();

            if ($this->isAddressSupported($address)) {
                $resultJson["success"] = true;
            } else {
                $resultJson["error"] = __("We are sorry, but this area is not supported by our service.");
            }

        } catch (\Exception $e) {
            $resultJson["error"] = $e->getMessage();
        }
        return $resultJson;
    }

    public function isAddressSupported($address)
    {
        return strtolower($address->getAddress()) == "Milan";
    }

    /**
     * @return DataObject
     * @throws \Exception
     */
    public function validatedRequestedAddress() {
        $request = $this->getRequest();
        $postalCode1 = $request->getParam(self::POSTAL_CODE_1_PARAM_NAME);
        $postalCode2 = $request->getParam(self::POSTAL_CODE_2_PARAM_NAME);
        $address = $request->getParam(self::ADDRESS_PARAM_NAME);
        if (empty($postalCode1))
            throw new \Exception(__(self::PARAM_MISSED_MESSAGE, self::POSTAL_CODE_1_PARAM_NAME));
        if (empty($postalCode2))
            throw new \Exception(__(self::PARAM_MISSED_MESSAGE, self::POSTAL_CODE_2_PARAM_NAME));
        if (empty($address))
            throw new \Exception(__(self::PARAM_MISSED_MESSAGE, self::ADDRESS_PARAM_NAME));

        return new DataObject([
            self::POSTAL_CODE_1_PARAM_NAME => $postalCode1,
            self::POSTAL_CODE_2_PARAM_NAME => $postalCode2,
            self::ADDRESS_PARAM_NAME => $address,
        ]);
    }
}
