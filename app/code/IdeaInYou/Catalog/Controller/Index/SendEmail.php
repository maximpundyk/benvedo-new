<?php

namespace IdeaInYou\Catalog\Controller\Index;

use IdeaInYou\Catalog\Model\Session;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class SendEmail extends Action implements ActionInterface
{
    const PARAM_MISSED_MESSAGE = "Error: %1 parameter is missed!";
    const EMAIL_PARAM_NAME = "email";
    protected $session;

    public function __construct(
        Session $session,
        Context $context
    )
    {
        $this->session = $session;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $email = $this->validatedRequest();

            $this->sendEmail($email);

            $resultJson["success"] = true;
        } catch (\Exception $e) {
            $resultJson["error"] = $e->getMessage();
        }
        return $resultJson;
    }

    public function sendEmail($email)
    {
        return true;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function validatedRequest() {
        $request = $this->getRequest();
        $email = $request->getParam(self::EMAIL_PARAM_NAME);
        if (empty($email))
            throw new \Exception(__(self::PARAM_MISSED_MESSAGE, self::EMAIL_PARAM_NAME));

        return $email;
    }
}
