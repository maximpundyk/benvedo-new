<?php
namespace IdeaInYou\Catalog\Controller\Index;

use IdeaInYou\Catalog\Model\Session;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\SessionException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;

class Index extends Action
{
    protected $pageFactory;

    protected $session;

    public function __construct(
        Session $session,
        Context $context,
        PageFactory $pageFactory
    ) {
        $this->session = $session;
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        try {
            $this->session->start()->clearStorage();
        } catch (SessionException $e) {
            throw new NotFoundException(__('Session couldn\'t been started!'));
        }
        return $this->pageFactory->create();
    }


}
