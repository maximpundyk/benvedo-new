<?php


namespace IdeaInYou\Catalog\Controller\Adminhtml\Category\BrandLogo;

use IdeaInYou\Catalog\Setup\UpgradeData;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ImageUploader;

class Upload extends Action
{
    protected $imageUploader;
    protected $_authorization;

    public function __construct(
        Context $context,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Catalog::categories');
    }

    public function execute()
    {
        try {
            $result = $this->imageUploader->saveFileToTmpDir(UpgradeData::BRAND_LOGO_IMAGE_ATTRIBUTE_CODE);

            $result['cookie'] = [
                'name' 	=> $this->_getSession()->getName(),
                'value' 	=> $this->_getSession()->getSessionId(),
                'lifetime'   => $this->_getSession()->getCookieLifetime(),
                'path'       => $this->_getSession()->getCookiePath(),
                'domain'   => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}