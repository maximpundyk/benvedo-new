<?php

namespace IdeaInYou\Catalog\Model\Attribute;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\UrlInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\Category\DataProvider as MagentoDataProvider;

class DataProvider extends MagentoDataProvider
{
    protected $_storeManager;

    protected function addUseDefaultSettings($category, $categoryData)
    {
        $data = parent::addUseDefaultSettings($category, $categoryData);

        if (isset($data['image_thumb'])) {
            unset($data['image_thumb']);

            $objectManager = ObjectManager::getInstance();
            $helper           	= $objectManager->get('\[ Vendor ]\[ Module ]\Helper\Data');

            $data['image_thumb'][0]['name'] = $category->getData('image_thumb');
            $data['image_thumb'][0]['url']  	= $helper->getCategoryThumbUrl($category);
        }

        return $data;
    }

    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'image_thumb'; // NEW FIELD

        return $fields;
    }

    public function getCategoryThumbUrl($category)
    {
        $url   = false;
        $image = $category->getImageThumb();
        if ($image) {
            if (is_string($image)) {
                $url = $this->_storeManager->getStore()->getBaseUrl(
                    UrlInterface::URL_TYPE_MEDIA
                    ) . 'catalog/category/' . $image;
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }

        return $url;
    }
}