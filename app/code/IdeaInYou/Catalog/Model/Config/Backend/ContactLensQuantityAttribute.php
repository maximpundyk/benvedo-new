<?php

namespace IdeaInYou\Catalog\Model\Config\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

class ContactLensQuantityAttribute extends AbstractBackend
{
    const LEFT_BORDER = 1;
    const RIGHT_BORDER = 10;
    const STEP = 1;

    /**
     * @param DataObject $object
     *
     * @return $this
     */
    public function afterLoad($object)
    {
        // your after load logic

        return parent::afterLoad($object);
    }

    /**
     * @param DataObject $object
     *
     * @return $this
     * @throws LocalizedException
     */
    public function beforeSave($object)
    {
        $this->validateValue($object);
        return parent::beforeSave($object);
    }

    /**
     * Validate length
     *
     * @param DataObject $object
     *
     * @return bool
     * @throws LocalizedException
     */
    public function validateValue($object)
    {
        /** @var string $attributeCode */
        $attributeCode = $this->getAttribute()->getAttributeCode();

        $value = (int)$object->getData($attributeCode);

        if ($this->getAttribute()->getIsRequired() && $value < self::LEFT_BORDER && $value > self::RIGHT_BORDER) {
            throw new LocalizedException (
                __("Contact Lens Quantity Attribute (%1) must have the value from %2 to %3 !", $attributeCode, self::LEFT_BORDER, self::RIGHT_BORDER)
            );
        }

        return true;
    }
}