<?php


namespace IdeaInYou\Catalog\Model\Config;


class Brands
{
    const TOMMY_HILFIGER = "Tommy Hilfiger";
    const TOM_FORD = "Tom Ford";
    const CALVIN_KLEIN= "Calvin Klein";
    const BOSS = "BOSS";
    const MARC_JACOBS = "Marc Jacobs";
    const VOGUE_EYEWEAR = "VOGUE Eyewear";
    const SAINT_LAURENT = "Saint Laurent";
    const EMPORIO_ARMANI = "Emporio Armani";
    const POLICE = "Police";
    const POLO_RALPH_LAUREN = "Polo Ralph Lauren";
    const BOSS_ORANGE = "BOSS ORANGE";
    const RODENSTOCK = "Rodenstock";
    const DIOR = "Dior";
    const MICHAEL_KORS = "Michael Kors";
    const PORSCHE_DESIGN = "Porsche Design";
    const JOS_ESCHENBACH = "Jos. Eschenbach";
    const RALPH = "Ralph";
    const DIESEL = "Diesel";
    const POLAROID = "Polaroid";
    const FENDI = "Fendi";
    const PRADA_LINEA_ROSSA = "Prada Linea Rossa";
    const HUGO_BOSS = "Hugo Boss";
    const BVLGARI = "Bvlgari";
    const MIU_MIU = "Miu Miu";
    const JUST_CAVALLI = "Just Cavalli";
    const RALPH_LAUREN = "Ralph Lauren";
    const MARC_BY_MARC_JACOBS = "Marc by Marc Jacobs";


    public static function getBrands() {
        return [

            [
                "name" => Brands::TOMMY_HILFIGER, //TOMMY_HILFIGER
                "url_key" => strtolower(str_replace(" ", "-", Brands::TOMMY_HILFIGER)),//tommy-hilfiger
                "title" => Brands::TOMMY_HILFIGER,
                "status" => 1
            ],

            [
                "name" => Brands::TOM_FORD,
                "url_key" => strtolower(str_replace(" ", "-", Brands::TOM_FORD)),
                "title" => Brands::TOM_FORD,
                "status" => 1
            ],

            [
                "name" => Brands::CALVIN_KLEIN,
                "url_key" => strtolower(str_replace(" ", "-", Brands::CALVIN_KLEIN)),
                "title" => Brands::CALVIN_KLEIN,
                "status" => 1
            ],

            [
                "name" => Brands::BOSS,
                "url_key" => strtolower(str_replace(" ", "-", Brands::BOSS)),
                "title" => Brands::BOSS,
                "status" => 1
            ],

            [
                "name" => Brands::MARC_JACOBS,
                "url_key" => strtolower(str_replace(" ", "-", Brands::MARC_JACOBS)),
                "title" => Brands::MARC_JACOBS,
                "status" => 1
            ],

            [
                "name" => Brands::VOGUE_EYEWEAR,
                "url_key" => strtolower(str_replace(" ", "-", Brands::VOGUE_EYEWEAR)),
                "title" => Brands::VOGUE_EYEWEAR,
                "status" => 1
            ],

            [
                "name" => Brands::SAINT_LAURENT,
                "url_key" => strtolower(str_replace(" ", "-", Brands::SAINT_LAURENT)),
                "title" => Brands::SAINT_LAURENT,
                "status" => 1
            ],

            [
                "name" => Brands::EMPORIO_ARMANI,
                "url_key" => strtolower(str_replace(" ", "-", Brands::EMPORIO_ARMANI)),
                "title" => Brands::EMPORIO_ARMANI,
                "status" => 1
            ],

            [
                "name" => Brands::POLICE,
                "url_key" => strtolower(str_replace(" ", "-", Brands::POLICE)),
                "title" => Brands::POLICE,
                "status" => 1
            ],

            [
                "name" => Brands::POLO_RALPH_LAUREN,
                "url_key" => strtolower(str_replace(" ", "-", Brands::POLO_RALPH_LAUREN)),
                "title" => Brands::POLO_RALPH_LAUREN,
                "status" => 1
            ],

            [
                "name" => Brands::BOSS_ORANGE,
                "url_key" => strtolower(str_replace(" ", "-", Brands::BOSS_ORANGE)),
                "title" => Brands::BOSS_ORANGE,
                "status" => 1
            ],

            [
                "name" => Brands::RODENSTOCK,
                "url_key" => strtolower(str_replace(" ", "-", Brands::RODENSTOCK)),
                "title" => Brands::RODENSTOCK,
                "status" => 1
            ],

            [
                "name" => Brands::DIOR,
                "url_key" => strtolower(str_replace(" ", "-", Brands::DIOR)),
                "title" => Brands::DIOR,
                "status" => 1
            ],

            [
                "name" => Brands::MICHAEL_KORS,
                "url_key" => strtolower(str_replace(" ", "-", Brands::MICHAEL_KORS)),
                "title" => Brands::MICHAEL_KORS,
                "status" => 1
            ],

            [
                "name" => Brands::PORSCHE_DESIGN,
                "url_key" => strtolower(str_replace(" ", "-", Brands::PORSCHE_DESIGN)),
                "title" => Brands::PORSCHE_DESIGN,
                "status" => 1
            ],

            [
                "name" => Brands::JOS_ESCHENBACH,
                "url_key" => strtolower(str_replace(" ", "-", Brands::JOS_ESCHENBACH)),
                "title" => Brands::JOS_ESCHENBACH,
                "status" => 1
            ],

            [
                "name" => Brands::RALPH,
                "url_key" => strtolower(str_replace(" ", "-", Brands::RALPH)),
                "title" => Brands::RALPH,
                "status" => 1
            ],

            [
                "name" => Brands::DIESEL,
                "url_key" => strtolower(str_replace(" ", "-", Brands::DIESEL)),
                "title" => Brands::DIESEL,
                "status" => 1
            ],

            [
                "name" => Brands::POLAROID,
                "url_key" => strtolower(str_replace(" ", "-", Brands::POLAROID)),
                "title" => Brands::POLAROID,
                "status" => 1
            ],

            [
                "name" => Brands::FENDI,
                "url_key" => strtolower(str_replace(" ", "-", Brands::FENDI)),
                "title" => Brands::FENDI,
                "status" => 1
            ],

            [
                "name" => Brands::PRADA_LINEA_ROSSA,
                "url_key" => strtolower(str_replace(" ", "-", Brands::PRADA_LINEA_ROSSA)),
                "title" => Brands::PRADA_LINEA_ROSSA,
                "status" => 1
            ],

            [
                "name" => Brands::HUGO_BOSS,
                "url_key" => strtolower(str_replace(" ", "-", Brands::HUGO_BOSS)),
                "title" => Brands::HUGO_BOSS,
                "status" => 1
            ],

            [
                "name" => Brands::BVLGARI,
                "url_key" => strtolower(str_replace(" ", "-", Brands::BVLGARI)),
                "title" => Brands::BVLGARI,
                "status" => 1
            ],

            [
                "name" => Brands::MIU_MIU,
                "url_key" => strtolower(str_replace(" ", "-", Brands::MIU_MIU)),
                "title" => Brands::MIU_MIU,
                "status" => 1
            ],

            [
                "name" => Brands::JUST_CAVALLI,
                "url_key" => strtolower(str_replace(" ", "-", Brands::JUST_CAVALLI)),
                "title" => Brands::JUST_CAVALLI,
                "status" => 1
            ],

            [
                "name" => Brands::RALPH_LAUREN,
                "url_key" => strtolower(str_replace(" ", "-", Brands::RALPH_LAUREN)),
                "title" => Brands::RALPH_LAUREN,
                "status" => 1
            ],

            [
                "name" => Brands::MARC_BY_MARC_JACOBS,
                "url_key" => strtolower(str_replace(" ", "-", Brands::MARC_BY_MARC_JACOBS)),
                "title" => Brands::MARC_BY_MARC_JACOBS,
                "status" => 1
            ],
        ];
    }
}