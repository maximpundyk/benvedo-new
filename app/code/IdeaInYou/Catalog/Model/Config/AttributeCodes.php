<?php


namespace IdeaInYou\Catalog\Model\Config;


use IdeaInYou\Catalog\Model\Config\Backend\AddAttribute;
use IdeaInYou\Catalog\Model\Config\Backend\AxisAttribute;
use IdeaInYou\Catalog\Model\Config\Backend\ContactLensAxisAttribute;
use IdeaInYou\Catalog\Model\Config\Backend\ContactLensDiopterAttribute;
use IdeaInYou\Catalog\Model\Config\Backend\ContactLensQuantityAttribute;
use IdeaInYou\Catalog\Model\Config\Backend\CylinderAttribute;
use IdeaInYou\Catalog\Model\Config\Backend\SphereAttribute;
use IdeaInYou\Catalog\Model\Config\Source\AsianFittingOptions;
use IdeaInYou\Catalog\Model\Config\Source\AxisOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensAddOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensBaseCurveOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensColorOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensCylinderOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensesDurationOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensesForAstigmatismOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensesPackageSizeOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensesTypeOptions;
use IdeaInYou\Catalog\Model\Config\Source\CylinderOptions;
use IdeaInYou\Catalog\Model\Config\Source\FaceShapeOptions;
use IdeaInYou\Catalog\Model\Config\Source\FrameMaterialOptions;
use IdeaInYou\Catalog\Model\Config\Source\FrameTypeOptions;
use IdeaInYou\Catalog\Model\Config\Source\FrontShapeOptions;
use IdeaInYou\Catalog\Model\Config\Source\HeadWidthOptions;
use IdeaInYou\Catalog\Model\Config\Source\LensColorOptions;
use IdeaInYou\Catalog\Model\Config\Source\MultifocalAvailableOptions;
use IdeaInYou\Catalog\Model\Config\Source\SettingLensesCoatingOptions;
use IdeaInYou\Catalog\Model\Config\Source\SettingLensesFieldOfVisionOptions;
use IdeaInYou\Catalog\Model\Config\Source\SettingLensesQualityOptions;
use IdeaInYou\Catalog\Model\Config\Source\SettingLensesTintOptions;
use IdeaInYou\Catalog\Model\Config\Source\SettingLensesTypeOptions;
use IdeaInYou\Catalog\Model\Config\Source\SexOptions;
use IdeaInYou\Catalog\Model\Config\Source\SphereOptions;
use IdeaInYou\Catalog\Model\Config\Source\TryAtHomeOptions;
use IdeaInYou\Catalog\Model\Config\Source\VirtualTryOnOptions;
use IdeaInYou\Catalog\Model\Config\Source\WorkplaceAvailableOptions;
use IdeaInYou\Catalog\Model\Config\Source\AddOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensDiopterOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensAxisOptions;
use IdeaInYou\Catalog\Model\Config\Source\ContactLensQuantityOptions;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class AttributeCodes
{
    //input "select"
    const FRAME_TYPE_ATTRIBUTE_CODE = "frame_type";
    const FACE_SHAPE_ATTRIBUTE_CODE = "face_shape";
    const SETTING_LENSES_TYPE_ATTRIBUTE_CODE = "setting_lenses_type";
    const SETTING_LENSES_COATING_ATTRIBUTE_CODE = "setting_lenses_coating";
    const SETTING_LENSES_TINT_ATTRIBUTE_CODE = "setting_lenses_tint";
    const SETTING_LENSES_QUALITY_ATTRIBUTE_CODE = "setting_lenses_quality";
    const SEX_ATTRIBUTE_CODE = "sex";
    const SETTING_LENSES_FIELDOFVISION_ATTRIBUTE_CODE = "setting_lenses_fieldOfVision";
    const MULTIFOCAL_AVAILABLE_ATTRIBUTE_CODE = "multifocal_available";
    const TRY_AT_HOME_ATTRIBUTE_CODE = "try_at_home";
    const ASIAN_FITTING_ATTRIBUTE_CODE = "asian_fitting";
    const WORKPLACE_AVAILABLE_ATTRIBUTE_CODE = "workplace_available";
    const VIRTUAL_TRY_ON_ATTRIBUTE_CODE = "virtual_try_on";
    const FRAME_MATERIAL_ATTRIBUTE_CODE = "frame_material";
    const FRONT_SHAPE_ATTRIBUTE_CODE = "front_shape";
    const HEAD_WIDTH_ATTRIBUTE_CODE = "head_width";
    const LENS_COLOR_ATTRIBUTE_CODE = "lens_color";
    const CONTACT_LENSES_TYPE_ATTRIBUTE_CODE = "contact_lenses_type";
    const CONTACT_LENSES_DURATION_ATTRIBUTE_CODE = "contact_lenses_duration";
    const RIGHT_CONTACTLENS_CYLINDER_ATTRIBUTE_CODE = "right_contactlens_cylinder";
    const LEFT_CONTACTLENS_CYLINDER_ATTRIBUTE_CODE = "left_contactlens_cylinder";
    const RIGHT_CONTACTLENS_ADD_ATTRIBUTE_CODE = "right_contactlens_add";
    const LEFT_CONTACTLENS_ADD_ATTRIBUTE_CODE = "left_contactlens_add";
    const RIGHT_CONTACTLENS_BASE_CURVE_ATTRIBUTE_CODE = "right_contactlens_base_curve";
    const LEFT_CONTACTLENS_BASE_CURVE_ATTRIBUTE_CODE = "left_contactlens_base_curve";
    const CONTACT_LENSES_FOR_ASIGMATISM_ATTRIBUTE_CODE = "contact_lenses_for_astigmatism";
    const RIGHT_CONTACTLENS_COLOR_ATTRIBUTE_CODE = "right_contactlens_color";
    const LEFT_CONTACTLENS_COLOR_ATTRIBUTE_CODE = "left_contactlens_color";

    //input "text"
    const FRAME_COLOR_ATTRIBUTE_CODE = "frame_color";
    const BRIDGE_ATTRIBUTE_CODE = "bridge";
    const LENS_WIDTH_ATTRIBUTE_CODE = "lens_width";
    const LENS_HEIGHT_ATTRIBUTE_CODE = "lens_height";
    const PANTOSCOPIC_ANGLE_ATTRIBUTE_CODE = "pantoscopic_angle";
    const FRAME_WIDTH_ATTRIBUTE_CODE = "frame_width";
    const TEMPLE_LENGTH_ATTRIBUTE_CODE = "temple_length";
    const WEIGHT_ATTRIBUTE_CODE = "weight";
    const WEIGHT_TYPE_ATTRIBUTE_CODE = "weight_type";
    const SIZE_ATTRIBUTE_CODE = "size";
    const SKU_ATTRIBUTE_CODE = "sku";
    const PRICE_ATTRIBUTE_CODE = "price";
    const STATUS_ATTRIBUTE_CODE = "status";
    const NAME_ATTRIBUTE_CODE = "name";
    const BRAND_ATTRIBUTE_CODE = "brand_id";
    const CONTACT_LENSES_PACKAGE_SIZE_ATTRIBUTE_CODE = "contact_lenses_package_size";

    //backend validation const
    const RIGHTEYE_SPHERE_ATTRIBUTE_CODE = "righteye_sphere";
    const LEFTEYE_SPHERE_ATTRIBUTE_CODE = "lefteye_sphere";
    const RIGHTEYE_CYLINDER_ATTRIBUTE_CODE = "righteye_cylinder";
    const LEFTEYE_CYLINDER_ATTRIBUTE_CODE = "lefteye_cylinder";
    const RIGHTEYE_AXIS_ATTRIBUTE_CODE = "righteye_axis";
    const LEFTEYE_AXIS_ATTRIBUTE_CODE = "lefteye_axis";
    const RIGHTEYE_ADD_ATTRIBUTE_CODE = "righteye_add";
    const LEFTEYE_ADD_ATTRIBUTE_CODE = "lefteye_add";
    const RIGHT_CONTACTLENS_DIOPTER_ATTRIBUTE_CODE = "right_contactlens_diopter";
    const LEFT_CONTACTLENS_DIOPTER_ATTRIBUTE_CODE = "left_contactlens_diopter";
    const RIGHT_CONTACTLENS_AXIS_ATTRIBUTE_CODE = "right_contactlens_axis";
    const LEFT_CONTACTLENS_AXIS_ATTRIBUTE_CODE = "left_contactlens_axis";
    const RIGHT_CONTACTLENS_QUANTITY_ATTRIBUTE_CODE = "right_contactlens_quantity";
    const LEFT_CONTACTLENS_QUANTITY_ATTRIBUTE_CODE = "left_contactlens_quantity";

    public static function getContactLensesAttributeSetCodes() {
        return [
            AttributeCodes::CONTACT_LENSES_TYPE_ATTRIBUTE_CODE,
            AttributeCodes::CONTACT_LENSES_DURATION_ATTRIBUTE_CODE,
            AttributeCodes::CONTACT_LENSES_PACKAGE_SIZE_ATTRIBUTE_CODE,
            AttributeCodes::RIGHT_CONTACTLENS_DIOPTER_ATTRIBUTE_CODE,
            AttributeCodes::LEFT_CONTACTLENS_DIOPTER_ATTRIBUTE_CODE,
            AttributeCodes::RIGHT_CONTACTLENS_CYLINDER_ATTRIBUTE_CODE,
            AttributeCodes::LEFT_CONTACTLENS_CYLINDER_ATTRIBUTE_CODE,
            AttributeCodes::RIGHT_CONTACTLENS_AXIS_ATTRIBUTE_CODE,
            AttributeCodes::LEFT_CONTACTLENS_AXIS_ATTRIBUTE_CODE,
            AttributeCodes::RIGHT_CONTACTLENS_ADD_ATTRIBUTE_CODE,
            AttributeCodes::LEFT_CONTACTLENS_ADD_ATTRIBUTE_CODE,
            AttributeCodes::RIGHT_CONTACTLENS_COLOR_ATTRIBUTE_CODE,
            AttributeCodes::LEFT_CONTACTLENS_COLOR_ATTRIBUTE_CODE,
            AttributeCodes::RIGHT_CONTACTLENS_BASE_CURVE_ATTRIBUTE_CODE,
            AttributeCodes::LEFT_CONTACTLENS_BASE_CURVE_ATTRIBUTE_CODE,
            AttributeCodes::RIGHT_CONTACTLENS_QUANTITY_ATTRIBUTE_CODE,
            AttributeCodes::LEFT_CONTACTLENS_QUANTITY_ATTRIBUTE_CODE,
            AttributeCodes::CONTACT_LENSES_FOR_ASIGMATISM_ATTRIBUTE_CODE,
        ];
    }

    public static function getGlassesAttributeSetCodes() {
        return [
            AttributeCodes::NAME_ATTRIBUTE_CODE,
            AttributeCodes::SKU_ATTRIBUTE_CODE,
            AttributeCodes::PRICE_ATTRIBUTE_CODE,
            AttributeCodes::STATUS_ATTRIBUTE_CODE,
            AttributeCodes::HEAD_WIDTH_ATTRIBUTE_CODE,
            AttributeCodes::BRAND_ATTRIBUTE_CODE,
            AttributeCodes::FRAME_TYPE_ATTRIBUTE_CODE,
            AttributeCodes::LENS_COLOR_ATTRIBUTE_CODE,
            AttributeCodes::LENS_HEIGHT_ATTRIBUTE_CODE,
            AttributeCodes::SEX_ATTRIBUTE_CODE,
            AttributeCodes::FRAME_MATERIAL_ATTRIBUTE_CODE,
            AttributeCodes::FRONT_SHAPE_ATTRIBUTE_CODE,
            AttributeCodes::FRAME_COLOR_ATTRIBUTE_CODE,
            AttributeCodes::MULTIFOCAL_AVAILABLE_ATTRIBUTE_CODE,
            AttributeCodes::FRAME_WIDTH_ATTRIBUTE_CODE,
            AttributeCodes::TEMPLE_LENGTH_ATTRIBUTE_CODE,
            AttributeCodes::BRIDGE_ATTRIBUTE_CODE,
            AttributeCodes::LENS_HEIGHT_ATTRIBUTE_CODE,
            AttributeCodes::LENS_WIDTH_ATTRIBUTE_CODE,
            AttributeCodes::FACE_SHAPE_ATTRIBUTE_CODE,
            AttributeCodes::TRY_AT_HOME_ATTRIBUTE_CODE,
            AttributeCodes::PANTOSCOPIC_ANGLE_ATTRIBUTE_CODE,
            AttributeCodes::ASIAN_FITTING_ATTRIBUTE_CODE,
            AttributeCodes::WORKPLACE_AVAILABLE_ATTRIBUTE_CODE,
            AttributeCodes::SETTING_LENSES_TYPE_ATTRIBUTE_CODE,
            AttributeCodes::SETTING_LENSES_QUALITY_ATTRIBUTE_CODE,
            AttributeCodes::SETTING_LENSES_COATING_ATTRIBUTE_CODE,
            AttributeCodes::SETTING_LENSES_TINT_ATTRIBUTE_CODE,
            AttributeCodes::SETTING_LENSES_FIELDOFVISION_ATTRIBUTE_CODE,
            AttributeCodes::VIRTUAL_TRY_ON_ATTRIBUTE_CODE,
            AttributeCodes::RIGHTEYE_SPHERE_ATTRIBUTE_CODE,
            AttributeCodes::LEFTEYE_SPHERE_ATTRIBUTE_CODE,
            AttributeCodes::RIGHTEYE_CYLINDER_ATTRIBUTE_CODE,
            AttributeCodes::LEFTEYE_CYLINDER_ATTRIBUTE_CODE,
            AttributeCodes::RIGHTEYE_AXIS_ATTRIBUTE_CODE,
            AttributeCodes::LEFTEYE_AXIS_ATTRIBUTE_CODE,
            AttributeCodes::RIGHTEYE_ADD_ATTRIBUTE_CODE,
            AttributeCodes::LEFTEYE_ADD_ATTRIBUTE_CODE,
            AttributeCodes::WEIGHT_ATTRIBUTE_CODE,
            AttributeCodes::WEIGHT_TYPE_ATTRIBUTE_CODE,
        ];
    }
    public static function getCareProductAttributeSetCodes() {
        return [
            AttributeCodes::SIZE_ATTRIBUTE_CODE,
            AttributeCodes::SKU_ATTRIBUTE_CODE,
            AttributeCodes::BRAND_ATTRIBUTE_CODE,
        ];
    }

    public static function getAttributesGeneralOptions() {
        return [
            'visible' => true,
            'user_defined' => true,
            'visible_on_front' => true,
            'visible_in_advanced_search' => true,
            'is_html_allowed_on_front' => true,
            'used_for_promo_rules' => true,
//                'visible_on_front' => true,
//                'used_defined' => false,
//                'used_in_product_listing' => false
        ];
    }
    public static function getAttributes() {
        return [
            //input "select"
            AttributeCodes::FRAME_TYPE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Frame Type',
                'source' => FrameTypeOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => false
            ],
            AttributeCodes::FACE_SHAPE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Face Shape',
                'source' => FaceShapeOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => false
            ],

            AttributeCodes::SETTING_LENSES_TYPE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => true,
                'label' => 'Setting Lenses Type',
                'source' => SettingLensesTypeOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::SETTING_LENSES_COATING_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Setting Lenses Coating',
                'source' => SettingLensesCoatingOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::SETTING_LENSES_TINT_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Setting Lenses Tint',
                'source' => SettingLensesTintOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::SETTING_LENSES_QUALITY_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Setting Lenses Quality',
                'source' => SettingLensesQualityOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::SEX_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Sex',
                'source' => SexOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => false
            ],

            AttributeCodes::SETTING_LENSES_FIELDOFVISION_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Setting Lenses Field Of Vision',
                'source' => SettingLensesFieldOfVisionOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::MULTIFOCAL_AVAILABLE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Multifocal Available',
                'source' => MultifocalAvailableOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::TRY_AT_HOME_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Try At Home',
                'source' => TryAtHomeOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::ASIAN_FITTING_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Asian Fitting',
                'source' => AsianFittingOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::WORKPLACE_AVAILABLE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Workplace Available',
                'source' => WorkplaceAvailableOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::VIRTUAL_TRY_ON_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Virtual Try On',
                'source' => VirtualTryOnOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::FRAME_MATERIAL_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Frame Material',
                'source' => FrameMaterialOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::FRONT_SHAPE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Front Shape',
                'source' => FrontShapeOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => false
            ],

            AttributeCodes::HEAD_WIDTH_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Head Width',
                'source' => HeadWidthOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => false
            ],
//            Already exists
//            AttributeCodes::LENS_COLOR_ATTRIBUTE_CODE => [
//                'input' => 'select',
//                'type' => 'varchar',
//                'required' => false,
//                'label' => 'Lens Color',
//                'source' => LensColorOptions::class,
//                'frontend_class' => '',
//                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
//                'unique' => false,
//                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
//                'filterable' => true,
//                'filterable_in_search' => true,
//                'searchable' => true,
//                'visible_in_advanced_search' => true,
//                'comparable' => true
//            ],

            AttributeCodes::CONTACT_LENSES_TYPE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Contact Lenses Type',
                'source' => ContactLensesTypeOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::CONTACT_LENSES_DURATION_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Contact Lenses Duration',
                'source' => ContactLensesDurationOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::RIGHT_CONTACTLENS_CYLINDER_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Right Contact Lens Cylinder',
                'source' => ContactLensCylinderOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFT_CONTACTLENS_CYLINDER_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Left Contact Lens Cylinder',
                'source' => ContactLensCylinderOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHT_CONTACTLENS_ADD_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Right Contact Lens Add',
                'source' => ContactLensAddOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFT_CONTACTLENS_ADD_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Left Contact Lens Add',
                'source' => ContactLensAddOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHT_CONTACTLENS_BASE_CURVE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Right Contact Lens Base Curve',
                'source' => ContactLensBaseCurveOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFT_CONTACTLENS_BASE_CURVE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Left Contact Lens Base Curve',
                'source' => ContactLensBaseCurveOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::CONTACT_LENSES_FOR_ASIGMATISM_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Contact Lenses For Astigmatism',
                'source' => ContactLensesForAstigmatismOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => false,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::RIGHT_CONTACTLENS_COLOR_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Right Contact Lens Color',
                'source' => ContactLensColorOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFT_CONTACTLENS_COLOR_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Left Contact Lens Color',
                'source' => ContactLensColorOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],


            //input"text"
            AttributeCodes::FRAME_COLOR_ATTRIBUTE_CODE => [
                'input' => 'text',
                'type' => 'varchar',
                'required' => false,
                'label' => 'Frame Color',
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => true,
                'filterable_in_search' => true,
                'searchable' => true,
                'visible_in_advanced_search' => true,
                'comparable' => true
            ],

            AttributeCodes::BRIDGE_ATTRIBUTE_CODE => [
                'input' => 'text',
                'type' => 'int',
                'required' => false,
                'label' => 'Bridge',
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LENS_HEIGHT_ATTRIBUTE_CODE => [
                'input' => 'text',
                'type' => 'int',
                'required' => false,
                'label' => 'Lens Height',
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LENS_WIDTH_ATTRIBUTE_CODE => [
                'input' => 'text',
                'type' => 'int',
                'required' => false,
                'label' => 'Lens Width',
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::PANTOSCOPIC_ANGLE_ATTRIBUTE_CODE => [
                'input' => 'text',
                'type' => 'int',
                'required' => false,
                'label' => 'Pantoscopic Angle',
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::FRAME_WIDTH_ATTRIBUTE_CODE => [
                'input' => 'text',
                'type' => 'int',
                'required' => false,
                'label' => 'Frame Width',
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_on_front' => true,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::TEMPLE_LENGTH_ATTRIBUTE_CODE => [
                'input' => 'text',
                'type' => 'int',
                'required' => false,
                'label' => 'Temple Length',
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::CONTACT_LENSES_PACKAGE_SIZE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'required' => false,
                'label' => 'Contact Lenses Package Size',
                'source' => ContactLensesPackageSizeOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],


            //backend validation
            AttributeCodes::RIGHTEYE_SPHERE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => SphereAttribute::class,
                'required' => false,
                'label' => 'Righteye Sphere',
                'source' => SphereOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFTEYE_SPHERE_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => SphereAttribute::class,
                'required' => false,
                'label' => 'Lefteye Sphere',
                'source' => SphereOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHTEYE_CYLINDER_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => CylinderAttribute::class,
                'required' => false,
                'label' => 'Righteye Cylinder',
                'source' => CylinderOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFTEYE_CYLINDER_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => CylinderAttribute::class,
                'required' => false,
                'label' => 'Lefteye Cylinder',
                'source' => CylinderOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHTEYE_AXIS_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => AxisAttribute::class,
                'required' => false,
                'label' => 'Righteye Axis',
                'source' => AxisOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFTEYE_AXIS_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => AxisAttribute::class,
                'required' => false,
                'label' => 'Lefteye Axis',
                'source' => AxisOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHTEYE_ADD_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => AddAttribute::class,
                'required' => false,
                'label' => 'Righteye Add',
                'source' => AddOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFTEYE_ADD_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => AddAttribute::class,
                'required' => false,
                'label' => 'Lefteye Add',
                'source' => AddOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHT_CONTACTLENS_DIOPTER_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => ContactLensDiopterAttribute::class,
                'required' => false,
                'label' => 'Right Contact Lens Diopter',
                'source' => ContactLensDiopterOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFT_CONTACTLENS_DIOPTER_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => ContactLensDiopterAttribute::class,
                'required' => false,
                'label' => 'Left Contac Lens Diopter',
                'source' => ContactLensDiopterOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHT_CONTACTLENS_AXIS_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => ContactLensAxisAttribute::class,
                'required' => false,
                'label' => 'Right Contact Lens Axis',
                'source' => ContactLensAxisOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFT_CONTACTLENS_AXIS_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => ContactLensAxisAttribute::class,
                'required' => false,
                'label' => 'Left Contact Lens Axis',
                'source' => ContactLensAxisOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::RIGHT_CONTACTLENS_QUANTITY_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => ContactLensQuantityAttribute::class,
                'required' => false,
                'label' => 'Right Contact Lens Quantity',
                'source' => ContactLensQuantityOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ],

            AttributeCodes::LEFT_CONTACTLENS_QUANTITY_ATTRIBUTE_CODE => [
                'input' => 'select',
                'type' => 'int',
                'backend' => ContactLensQuantityAttribute::class,
                'required' => false,
                'label' => 'Left Contact Lens Quantity',
                'source' => ContactLensQuantityOptions::class,
                'frontend_class' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'unique' => false,
                'apply_to' => 'simple,grouped,configurable,downloadable,virtual,bundle',
                'filterable' => false,
                'filterable_in_search' => false,
                'searchable' => false,
                'visible_in_advanced_search' => false,
                'comparable' => false
            ]
        ];
    }
}