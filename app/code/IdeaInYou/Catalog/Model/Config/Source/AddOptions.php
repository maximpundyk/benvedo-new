<?php


namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class AddOptions extends AbstractSource
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = ["value" => 0, "label" => 0];

        for ($i = 0.75, $j = 1; $i <= 3; $i += 0.25, $j++) {
            $result[] = ["value" => $j, "label" => strval($i)];
        }

        return $result;
    }

    /**
     * @param bool $withEmpty
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false): array
    {
        $result = ["value" => 0, "label" => 0];

        for ($i = 0.75, $j = 1; $i <= 3; $i += 0.25, $j++) {
            $result[] = ["label" => strval($i), "value" => $j];
        }
        $this->_options = $result;
        return $this->_options;
    }
}
