<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensBaseCurveOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('8.4'), 'value' => '0'],
            ['label' => __('8.6'), 'value' => '1']

        ];

        return $this->_options;
    }
}