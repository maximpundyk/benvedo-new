<?php


namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensAxisOptions extends AbstractSource
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        for ($i = 10; $i <= 180;  $i+=10) {
            $result[] = ["value" => $i, "label" => strval($i)];
        }

        return $result;
    }

    public function getAllOptions($withEmpty = true, $defaultValues = false): array
    {
        $result = [];

        for ($i = 10; $i <= 180;  $i+=10) {
            $result[] = ["value" => $i, "label" => strval($i)];
        }

        return $result;
    }
}