<?php


namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensQuantityOptions extends AbstractSource
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        for ($i = 1; $i <= 10;  $i++) {
            $result[] = ["value" => $i, "label" => strval($i)];
        }

        return $result;
    }

    public function getAllOptions($withEmpty = true, $defaultValues = false): array
    {
        $result = [];

        for ($i = 1; $i <= 10;  $i++) {
            $result[] = ["value" => $i, "label" => strval($i)];
        }

        return $result;
    }
}