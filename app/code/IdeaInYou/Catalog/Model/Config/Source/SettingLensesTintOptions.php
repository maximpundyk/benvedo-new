<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SettingLensesTintOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('No Tint'), 'value'=>'0'],
            ['label' => __('Sunglasses tint'), 'value'=>'1'],
            ['label' => __('Polarised lenses'), 'value'=>'2'],
            ['label' => __('Self-tinting lenses'), 'value'=>'3']
        ];

        return $this->_options;

    }

}