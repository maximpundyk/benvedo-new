<?php


namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class CylinderOptions extends AbstractSource
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        for ($i = -4, $j = 1; $i <= 4; $i += 0.25, $j++) {
            $result[] = ["value" => $j, "label" => strval($i)];
        }

        return $result;
    }

    public function getAllOptions($withEmpty = true, $defaultValues = false): array
    {
        $result = [];

        for ($i = -4, $j = 1; $i <= 4; $i += 0.25, $j++) {
            $result[] = ["label" => strval($i), "value" => $j];
        }

        $this->_options = $result;
        return $result;
    }

}