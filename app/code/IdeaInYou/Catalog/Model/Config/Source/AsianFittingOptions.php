<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class AsianFittingOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('No'), 'value'=>'0'],
            ['label' => __('Yes'), 'value'=>'1']
        ];

        return $this->_options;

    }

}