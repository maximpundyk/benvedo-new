<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SettingLensesTypeOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Multifocal'), 'value'=>'0'],
            ['label' => __('Single Vision'), 'value'=>'1'],
            ['label' => __('Workplace'), 'value'=>'2'],
            ['label' => __('Non-prescription lenses'), 'value'=>'3']
        ];

        return $this->_options;

    }

}

