<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensesTypeOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Toric'), 'value' => '0'],
            ['label' => __('Multifocal '), 'value' => '1'],
            ['label' => __('Colored'), 'value' => '2'],
            ['label' => __('Single vision'), 'value' => '3']

        ];

        return $this->_options;
    }
}