<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SettingLensesCoatingOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('No Coating'), 'value'=>'0'],
            ['label' => __('Super Hydrophobic '), 'value'=>'1'],
            ['label' => __('Blue Light Eye Protection'), 'value'=>'2']
        ];

        return $this->_options;

    }

}