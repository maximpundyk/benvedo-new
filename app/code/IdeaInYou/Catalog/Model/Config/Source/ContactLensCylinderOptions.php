<?php


namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensCylinderOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('-0.75'), 'value' => '0'],
            ['label' => __('-1.25 '), 'value' => '1'],
            ['label' => __('-1.75'), 'value' => '2'],
            ['label' => __('-2.25'), 'value' => '3']

        ];

        return $this->_options;
    }
}