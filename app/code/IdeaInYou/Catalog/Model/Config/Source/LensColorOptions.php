<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class LensColorOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Red'), 'value' => '0'],
            ['label' => __('Blue'), 'value' => '1'],
            ['label' => __('Black'), 'value' => '2']
        ];

        return $this->_options;
    }
}