<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SexOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Man'), 'value'=>'0'],
            ['label' => __('Woman'), 'value'=>'1'],
            ['label' => __('Unisex'), 'value'=>'2']
        ];

        return $this->_options;

    }

}

