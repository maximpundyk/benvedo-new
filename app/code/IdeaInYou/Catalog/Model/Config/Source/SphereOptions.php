<?php


namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SphereOptions extends AbstractSource
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        for ($i = -8, $j = 1; $i <= 6; $i += 0.25, $j++) {
            $result[] = ["value" => $j, "label" => strval($i)];
        }

        return $result;
    }

    /**
     * @param bool $withEmpty
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false): array
    {
        $option = [];

        for ($i = -8, $j = 1; $i <= 6; $i += 0.25, $j++) {
            $option[] = ["value" => $j, "label" => strval($i)];
        }

        $this->_options = $option;

        return $this->_options;
    }

}