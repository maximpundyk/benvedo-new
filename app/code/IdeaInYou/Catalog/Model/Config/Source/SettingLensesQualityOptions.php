<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SettingLensesQualityOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Standard'), 'value'=>'0'],
            ['label' => __('Comfort'), 'value'=>'1'],
            ['label' => __('Premium'), 'value'=>'2'],
            ['label' => __('Ultra-Premium'), 'value'=>'3']
        ];

        return $this->_options;

    }

}