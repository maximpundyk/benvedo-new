<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class FrontShapeOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Narrow'), 'value' => '0'],
            ['label' => __('Aviator'), 'value' => '1'],
            ['label' => __('Rectangle'), 'value' => '2'],
            ['label' => __('Butterfly/Cat-Eye'), 'value' => '3'],
            ['label' => __('Round'), 'value' => '4'],
            ['label' => __('Browline'), 'value' => '5']
        ];

        return $this->_options;
    }
}