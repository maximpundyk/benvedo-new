<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SettingLensesFieldOfVisionOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Normal Field Of Vision'), 'value'=>'0'],
            ['label' => __('Extended Field Of Vision'), 'value'=>'1'],
            ['label' => __('Maximum Field Of Vision'), 'value'=>'2']
        ];

        return $this->_options;

    }

}
