<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class FrameMaterialOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Carbon Fiber'), 'value' => '0'],
            ['label' => __('Aluminium '), 'value' => '1'],
            ['label' => __('Wood'), 'value' => '2'],
            ['label' => __('Metal'), 'value' => '3'],
            ['label' => __('Plastic'), 'value' => '4'],
            ['label' => __('Titanium'), 'value' => '5'],
            ['label' => __('Leather'), 'value' => '6']
        ];

        return $this->_options;
    }
}