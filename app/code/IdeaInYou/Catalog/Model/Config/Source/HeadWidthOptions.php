<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class HeadWidthOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Small'), 'value' => '0'],
            ['label' => __('Medium'), 'value' => '1'],
            ['label' => __('Large'), 'value' => '2']
        ];

        return $this->_options;
    }
}