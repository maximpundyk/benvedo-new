<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensAddOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('H'), 'value' => '0'],
            ['label' => __('L'), 'value' => '1'],
            ['label' => __('M'), 'value' => '2']

        ];

        return $this->_options;
    }
}