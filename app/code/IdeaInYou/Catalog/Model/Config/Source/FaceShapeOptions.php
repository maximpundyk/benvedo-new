<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class FaceShapeOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Heart Shaped'), 'value'=>'0'],
            ['label' => __('Oval'), 'value'=>'1'],
            ['label' => __('Round'), 'value'=>'2'],
            ['label' => __('Square'), 'value'=>'3']
        ];

        return $this->_options;

    }

}
