<?php


namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensDiopterOptions extends AbstractSource
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        for ($i = -10, $j = 1; $i <= 8; $i += 0.25, $j++) {
            $result[] = ["value" => $j, "label" => strval($i)];
        }

        return $result;
    }

    public function getAllOptions($withEmpty = true, $defaultValues = false): array
    {
        $result = [];

        for ($i = -10, $j = 1; $i <= 8; $i += 0.25, $j++) {
            $result[] = ["value" => $j, "label" => strval($i)];
        }

        return $result;
    }
}