<?php


namespace IdeaInYou\Catalog\Model\Config\Source;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensColorOptions extends AbstractSource
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [
            ["label" => "S", "value" => "S"],
        ];

        return $result;
    }

    /**
     * @param bool $withEmpty
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false): array
    {
        $this->_options = [
            ["label" => "S", "value" => "S"],
        ];

        return $this->_options;
    }
}