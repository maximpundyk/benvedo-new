<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class FrameTypeOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Rimless'), 'value'=>'0'],
            ['label' => __('Half-Rim'), 'value'=>'1'],
            ['label' => __('Full-Rim'), 'value'=>'2']
        ];

        return $this->_options;

    }

}