<?php

namespace IdeaInYou\Catalog\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class ContactLensesDurationOptions extends AbstractSource
{
    /**
     * @return array|array[]|null
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label' => __('Daily'), 'value' => '0'],
            ['label' => __('Weekly '), 'value' => '1'],
            ['label' => __('Monthly'), 'value' => '2'],
            ['label' => __('Day and Night'), 'value' => '3']

        ];

        return $this->_options;
    }
}