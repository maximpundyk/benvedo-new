define([
    'uiComponent',
    'ko',
    'IdeaInYou_Catalog/js/bpp/global',
    'IdeaInYou_Catalog/js/bpp/schedule_visit'
], function(Component, ko, global, schedule_visit) {
    let self;
    return Component.extend({
        defaults: {
            visible: ko.observable(false),
            emailSent: ko.observable(false),
        },

        formId: 'send-email-form',
        email: ko.observable(""),

        initialize: function () {
            this._super();
            self = this;

            schedule_visit().isAddressSupported.subscribe(function (value) {
                self.visible(value);
            });
        },

        sendEmail: function () {
            let form = jQuery('#'+self.formId);
            form.validation();

            if (!form.validation('isValid')) return;

            global().sendEmail(this.email())
                .then(function (data) {
                    console.log("Email Sent");
                    self.visible(false)
                    self.emailSent(true);
                });
        }
    });
});