define([
    'uiComponent',
    'ko',
    "jquery",
], function(Component, ko, $) {

    let ScheduledVisitAddress = function (postalCode1, postalCode2, address) {
        let self = this;
        self.postalCode1 = ko.observable(postalCode1);
        self.postalCode2 = ko.observable(postalCode2);
        self.adress = ko.observable(address);
    }


    return Component.extend({
        scheduleVisitAddressRoute: '',
        sendEmailRoute: '',
        loaderGif: '',

        $productOptions: ko.observable({}),
        $email: ko.observable(""),
        $scheduledVisitAddress: ko.observable(new ScheduledVisitAddress("", "")),

        initialize: function () {
            this._super();
            console.log("Passed options! :: " + this.scheduleVisitAddressRoute)
        },

        setProductOptions: function (options) {
            this.$productOptions(options);
        },

        getProductOptions: function () {
            return this.$productOptions();
        },

        setScheduledVisitAddress: function(postalCode, address) {
            let svAddress = ScheduledVisitAddress(postalCode, address);
            this.$scheduledVisitAddress(svAddress);
        },

        getScheduledVisitAddress: function() {
            return this.$scheduledVisitAddress;
        },

        setEmail: function(email) {
            this.$email(email);
        },

        getEmail: function(email) {
            return this.$email();
        },

        sendScheduledVisitAddress: async function (postalCode1,postalCode2, address) {
            this.$scheduledVisitAddress(new ScheduledVisitAddress(postalCode1, postalCode2, address));

            return $.ajax(this.scheduleVisitAddressRoute,{
                method: 'POST',
                data: {
                  postal_code_1: postalCode1,
                  postal_code_2: postalCode2,
                  address: address
                },
                showLoader: true
            }).done(function(data){
                if (data != null && data.error != null){
                    alert(data.error)
                    return false;
                }

                if (data.success) {
                    return true;
                }
            }).fail(function(err){
                console.error($t('Can not finish request.Try again.'));
                loader.stop()
            }).always(function(){

            });
        },

        sendEmail: async function (email) {
            this.setEmail(email);

            $.ajax(this.sendEmailRoute,{
                method: 'POST',
                data: {
                  email: email
                },
                showLoader: true
            }).done(function(data){
                if (data != null && data.error != null){
                    alert(data.error)
                    return false;
                }

                if (data.success) {
                    return true;
                }
            }).fail(function(){
                console.error($t('Can not finish request.Try again.'));
                loader.stop()
            }).always(function(){

            });
        }
    });
});