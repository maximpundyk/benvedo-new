define([
    'uiComponent',
    'ko',
    'IdeaInYou_Catalog/js/bpp/yes_no_question'
], function(Component, ko, yes_no_question) {
    let self;
    return Component.extend({
        visible: ko.observable(""),
        $selectedOption: ko.observable(""),
        selectedOption: '',

        choiceOptions: [
            {
                value: "1",
                text: "I want to schedule a visit to update or ask for prescription"
            },
            {
                value: "2",
                text: "I already have prescription"
            },
            {
                value: "3",
                text: "Add to Basket without prescription"
            }
        ],

        initialize: function () {
            this._super();
            self = this;

            yes_no_question().answer.subscribe(function(value) {
                self.visible(value === "yes");
            });
        },

        optionChanged: function(obj, event) {
            if (event.originalEvent) { //user changed

            } else { // program changed

            }
        },
        /**
         * Return true or false from previous Yes/No question
         * @returns true | false
         */
        getPrevAnswer: function() {
            return yes_no_question().getAnswer()
        },

        selectOption: function (item, event) {
            if (event.originalEvent) {
                self.selectedOption = item.value;
            }
        },

        nextStepClick: function () {
            self.visible(false);
            self.$selectedOption(self.selectedOption);
        }
    });
});