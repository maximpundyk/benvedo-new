define([
    'uiComponent',
    'ko',
    'mage/translate',
    'IdeaInYou_Catalog/js/bpp/global',
    'IdeaInYou_Catalog/js/bpp/select_option',
    "jquery",
    'mage/loader',
], function(Component, ko, $t, global, select_option, $, ) {
    let self;
    return Component.extend({
        blockSelector: "loader",
        scheduleVisitAddressRoute: '',

        defaults: {
            visible: ko.observable(false),
        },

        formId: 'address-form',
        postalCode1: ko.observable(""),
        postalCode2: ko.observable(""),
        address: ko.observable(""),

        isAddressSupported: ko.observable(""),
        // errors: ko.validation.group(this),

        initialize: function () {
            this._super();
            self = this;

            select_option().$selectedOption.subscribe(function(value) {
                self.visible(select_option().choiceOptions[0].value === value);
            });
        },

        setAddressInfo: function() {
            let form = jQuery('#'+self.formId);
            form.validation();

            if (!form.validation('isValid')) return;

            console.warn("Field 1 :: " + self.postalCode1());
            console.warn("Field 2 :: " + self.postalCode2());
            console.warn("Field 3 :: " + self.address());
            console.warn("VALidation passed!!!");

            global().sendScheduledVisitAddress(self.postalCode1(), self.postalCode2(), self.address())
                .then(function(result) {
                    if (result) {
                        self.visible(!result);
                        self.isAddressSupported(result);
                    }
                });
        }
    });
});