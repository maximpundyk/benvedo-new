define([
    'uiComponent',
    'ko'
], function(Component, ko) {
    return Component.extend({
        defaults: {
            answer: ko.observable("not-set"),
            visible: ko.observable(true),
        },

        initialize: function () {
            this._super();
            let _self = this;
            console.warn("Answer ::: " + this.answer());
            console.warn("Visible ::: " + this.visible());
        },

        setQuestionAnswer: function(answer) {
            if (answer) {
                this.answer("yes");
            } else {
                this.answer("no");
            }

            this.visible(false);
        },

        getAnswer: function () {
            return this.answer();
        },

        setAnswerTrue: function () {
            this.setQuestionAnswer(true);
        },

        setAnswerFalse: function () {
            this.setQuestionAnswer(false);
        }
    });
});