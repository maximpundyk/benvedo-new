<?php

namespace IdeaInYou\Catalog\Plugin\Helper\Product;

use Magento\Catalog\Helper\Product\View as Subject;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Framework\View\Result\Page as ResultPage;
use Magento\Catalog\Model\Product;

class View
{
    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    public function __construct(
        AttributeSetRepositoryInterface $attributeSetRepository
    )
    {
        $this->attributeSetRepository = $attributeSetRepository;
    }

    public function beforeInitProductLayout(
        Subject $view,
        ResultPage $resultPage,
        Product $product,
        $params = null
    ) {
        try {
            $attribute = $this->attributeSetRepository->get($product->getAttributeSetId());
            $attributeLayoutName = str_replace(' ', '_', strtolower($attribute->getAttributeSetName()));

            $resultPage->addPageLayoutHandles(
                ['attribute_set' => $attributeLayoutName]
            );

        } catch (\Exception $exception) {

        }

        return [$resultPage, $product, $params];
    }

}